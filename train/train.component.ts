import { DocService } from "../backend/doc.service";
import { ProjectService } from "../backend/project.service";
import { EntityName } from "../entity";
import { Project } from "../project/project.model";
import { ApiService } from "../utils";
import { ApiResponseError } from "../utils/api.service";
import { Component, ViewChild } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-train',
  templateUrl: './train.component.html',
  styleUrls: ['./train.component.scss']
})
export class TrainComponent {

  @ViewChild('tmplTrainInitial') tmplTrainInitial;

  selectedEntity: EntityName;
  projects: Project[] = [];
  selectedProjects: Project[];

  constructor(
    private modalService: NgbModal,
    private toastr: ToastrService,
    private projectService: ProjectService,
    private docService: DocService,
  ) {}

  trainInitialOpen(orig_entity: EntityName) {
    const entity = (this.selectedEntity = new EntityName(
      orig_entity || {name: '', complete: false}
    ));
    if (!this.projects.length) {
      this.getProjects();
    }
    const modal = this.modalService.open(this.tmplTrainInitial);
    modal.result.then(updated_category => {}).catch(() => {});
  }

  getProjects(): void {
    this.projectService
      .getProjects()
      .then(projects => {
        this.projects = projects;
      })
      .catch();
  }

  trainInitialSubmit(c) {
    const docsId = [];
    const targets = [this.selectedEntity.name];
    this.selectedProjects.forEach(p => {
      p.docs.forEach(d => {
        docsId.push(d.id);
      });
    });
    if (docsId.length) {
      this.docService
        .trainDocs(docsId, null, targets)
        .then(() => {
          this.toastr.success('Success');
          c();
        })
        .catch((error: ApiResponseError) => {
          const msg =
            (error.message && error.message.detail) ||
            ApiService.fieldError('', error) ||
            'Unknown error';
          this.toastr.warning(msg, error.message && error.message.title);
          c();
        });
    } else {
      this.toastr.warning('Selected projects not contain any documents.');
    }
  }
}
