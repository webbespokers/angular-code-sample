import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import {
  ApiService,
  ApiResponseError,
  FormHelperDirective,
  ConfirmService
} from '../utils';
import { EntityName } from '../entity/entity-name.model';
import { Project } from '../project/project.model';
import { EntityService, EntityFilter, EntityNameChartSearch } from '../backend/entity.service';
import { ProjectService, DocService } from '../backend';
import { TruncatePipe } from '../pipes/truncate.pipe';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent {

  @ViewChild('tmplEntityChart') tmplEntityChart;
  
  selectedEntity: EntityName;
  entityNameChartSearch: EntityNameChartSearch;
  precisionChartData: Array<any> = [{data: [], label: 'Precision'}];
  precisionChartLabels: Array<any> = [];
  recallChartData: Array<any> = [{data: [], label: 'Recall'}];
  recallChartLabels: Array<any> = [];
  accuracyChartData: Array<any> = [{data: [], label: 'Accuracy'}];
  accuracyChartLabels: Array<any> = [];
  lineChartOptions: any = {responsive: true, scales: {yAxes: [{ticks: {min: 0, max: 1}}]}};
  lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  lineChartLegend = false;
  lineChartType = 'line';
  constructor(
    private entityService: EntityService,
    private modalService: NgbModal
  ) {}

  showChart(entity: EntityName) {
    this.selectedEntity = new EntityName(entity || {name: '', complete: false});
    const modal = this.modalService.open(this.tmplEntityChart);
    modal.result.then(() => {},
      () => {
        this.resetCharts();
      }
    );

    this.entityNameChartSearch = {entity_name_id: entity.id};
    const precisionData = [];
    const recallData = [];
    const accuracyData = [];
    this.resetCharts();
    this.entityService.getEntityNameChart(this.entityNameChartSearch).then(chartData => {
      chartData.forEach(d => {
        switch (d.type) {
          case 'prec':
            precisionData.push(d.value);
            this.precisionChartLabels.push(d.date);
            break;
          case 'rec':
            recallData.push(d.value);
            this.recallChartLabels.push(d.date);
            break;
          case 'acc':
            accuracyData.push(d.value);
            this.accuracyChartLabels.push(d.date);
            break;
        }
      });
      this.precisionChartData = [{data: precisionData, label: 'Precision'}];
      this.recallChartData = [{data: recallData, label: 'Series A'}];
      this.accuracyChartData = [{data: accuracyData, label: 'accuracy'}];
    });
  }

  resetCharts() {
    this.precisionChartLabels.length = 0;
    this.recallChartLabels.length = 0;
    this.accuracyChartLabels.length = 0;
    this.precisionChartData = [{data: []}];
    this.recallChartData = [{data: []}];
    this.accuracyChartData = [{data: []}];
  }
}
