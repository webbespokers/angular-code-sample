**Description**
The sample code comes from the SaaS software project built to automate document analysis.
Software can process thousands of text documents to analize content, search common phrases, etc.

**Components description**

 - `chart` component - displays charts with data retrieved from api 
 - `doc` component - allows to create new document and uploads file(s) with progress bar
 - `entity-admin`, `entity-edit` components - entity CRUD and search
 - `train` component - mass processing of selected projects
