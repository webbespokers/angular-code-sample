import { Injectable, Inject, EventEmitter} from '@angular/core';
import { Http, URLSearchParams, ResponseContentType, RequestOptions } from "@angular/http";
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IAppConfig } from "../iappconfig.interface";
import { APP_CONFIG } from "../app.config";
import { Doc} from "../doc/doc.model";
import { Project} from "../project/project.model";
import { AuthOptions, getAuthKey } from "../auth/auth.service";
import * as FileSaver from 'file-saver';

interface DocFilter {
  search?: string;
  status?: string;
  project?: number;
  with_project?: boolean;
  mode?: 'short' | 'stat' | 'smer' | 'list' | 'smer-entities';
  limit?: number;
  offset?: number;
}

export interface DocServiceEvent {
  doc: Doc;
  action: 'create' | 'rename' | 'update' | 'delete';
}

@Injectable()
export class DocService {
  public uploadPressed: boolean;
  public docEvent = new EventEmitter<DocServiceEvent>();

  constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig,
              private http: Http,
              private httpClient: HttpClient
            ) {
  }

  public emitDoc(doc: Doc, action: 'create' | 'rename' | 'update' | 'delete') {
    this.docEvent.emit({doc: doc, action: action});
    return doc;
  }

  private requestOptions(filter: DocFilter): RequestOptions {
    let options = AuthOptions();
    if (filter) {
      options = options || new RequestOptions();
      const params = options.params = new URLSearchParams();
      if (filter.status) {
        params.set('status', filter.status);
      }
      if (filter.search) {
        params.set('search', filter.search);
      }
      if (filter.project) {
        params.set('project', filter.project.toString());
      }
      if (filter.with_project) {
        params.set('with_project', '1');
      }
      if (filter.mode) {
        params.set('mode', filter.mode);
      }
      if (filter.offset) {
        params.set('offset', filter.offset.toString());
      }
      if (filter.limit) {
        params.set('limit', filter.limit.toString());
      }
    }
    return options;
  }

  getDocs(filter: DocFilter = null): Promise<Doc[]> {
    return this.http.get(this.appConfig.apiEndpoint + "documents/", this.requestOptions(filter)).toPromise()
      .then(response => {
        const data = response.json();
        const paged = filter && filter.limit != null;
        const res = (paged ? data.results : data).map(obj => new Doc(obj));
        if (paged) {
          res.push(data.next ? filter.limit + (filter.offset || 0) : null);
        }
        return res;
      })
      .catch(this.handleError);
  }

  getXlsxReportProject(project: Project): void {
    var options = AuthOptions();
    options.responseType = ResponseContentType.Blob;

    this.http.get(this.appConfig.apiEndpoint + "projects/" + project.id + "/download_report/", options).toPromise()
      .then(response => {
        let fileBlob = response.blob();
        let blob = new Blob([fileBlob], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        let filename = project.title + ".xlsx";
        FileSaver.saveAs(blob, filename);
      });
  }

  getXlsxReport(doc: Doc): void {
    var options = AuthOptions();
    options.responseType = ResponseContentType.Blob;

    this.http.get(this.appConfig.apiEndpoint + "documents/" + doc.id + "/download_report/", options).toPromise()
      .then(response => {
        let fileBlob = response.blob();
        let blob = new Blob([fileBlob], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        let filename = doc.title + ".xlsx";
        FileSaver.saveAs(blob, filename);
      });
  }

  analyze(doc: Doc): Promise<Doc> {
    return this.http.post(this.appConfig.apiEndpoint + "documents/" + doc.id + "/analyze/", {}, AuthOptions()).toPromise()
      .then(response => new Doc(response.json()))
      .catch(this.handleError);
  }

  convert(doc: Doc): Promise<Doc> {
    return this.http.post(this.appConfig.apiEndpoint + "documents/" + doc.id + "/convert/", {}, AuthOptions()).toPromise()
      .then(response => new Doc(response.json()))
      .catch(this.handleError);
  }

  ml(doc: Doc): Promise<Doc> {
    return this.http.post(this.appConfig.apiEndpoint + "documents/" + doc.id + "/ml/", {}, AuthOptions()).toPromise()
      .then(response => new Doc(response.json()))
      .catch(this.handleError);
  }

  ai(doc: Doc[]): Promise<any> {
    const docIds = doc.map(d => d.id);
    const data = {
      doc_ids: docIds,
      auth: getAuthKey()
    };
    return this.http.post(this.appConfig.apiEndpoint + "documents/" + docIds[0] + "/run_ai/", data, AuthOptions()).toPromise()
      .then(response => response)
      .catch(this.handleError);
  }

  es(doc: Doc): Promise<Doc> {
    return this.http.post(this.appConfig.apiEndpoint + "documents/" + doc.id + "/es/", {}, AuthOptions()).toPromise()
      .then(response => new Doc(response.json()))
      .catch(this.handleError);
  }

  getDoc(id: number, filter: DocFilter = null): Promise<Doc> {
    return this.http.get(this.appConfig.apiEndpoint + "documents/" + id + "/", this.requestOptions(filter)).toPromise()
      .then(response => new Doc(response.json()))
      .catch(this.handleError);
  }

  getIHtml(id: number): Promise<any> {
    return this.http.get(this.appConfig.apiEndpoint + "documents/" + id + "/ihtml/", AuthOptions()).toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  saveDoc(doc: Doc, formData?: FormData): Promise<any> {
    if (doc.id) {
      return this.http.patch(this.appConfig.apiEndpoint + "documents/" + doc.id + "/", doc.forPost(), AuthOptions()).toPromise()
        .then(response => this.emitDoc(new Doc(response.json()), 'update'))
        .catch(this.handleError);
    }
  }

  addDoc(doc: Doc, formData?: FormData): Observable<any> {
    const currentKey = localStorage.getItem('currentKey');
    const request = new HttpRequest(
      "POST",
      this.appConfig.apiEndpoint + "documents/",
      formData || doc.forPost(),
      {
        reportProgress: true,
        headers: new HttpHeaders({'Authorization': 'Token ' + currentKey}),
      }
    );
    return this.httpClient.request(request);
  }

  patchDoc(dict: any): Promise<any> {
    if (dict.id) {
      return this.http.patch(this.appConfig.apiEndpoint + "documents/" + dict.id + "/", dict, AuthOptions()).toPromise()
        .then(response => this.emitDoc(new Doc(response.json()), 'update'))
        .catch(this.handleError);
    }
  }

  setChecked(doc: Doc, checked: boolean = true) {
    return this.http.patch(this.appConfig.apiEndpoint + "documents/" + doc.id + "/", {checked: checked}, AuthOptions()).toPromise()
      .then(response => this.emitDoc(new Doc(response.json()), 'update'))
      .catch(this.handleError);
  }

  deleteDoc(doc: Doc): Promise<any> {
    return this.http.delete(this.appConfig.apiEndpoint + "documents/" + doc.id + "/", AuthOptions()).toPromise()
      .then(response => {
        this.docEvent.emit({doc: doc, action: 'delete'});
        return response;
      })
      .catch(this.handleError);
  }

  renameDoc(doc: Doc): Promise<Doc> {
    return this.http.patch(this.appConfig.apiEndpoint + "documents/" + doc.id + "/rename/", doc.forPost(), AuthOptions()).toPromise()
      .then(response => this.emitDoc(new Doc(response.json()), 'rename'))
      .catch(this.handleError);
  }

  trainDocs(doc_ids, targets, type = 'ner', init = 1): Promise<any> {
    const data = {
      doc_ids: doc_ids,
      targets: targets,
      type: type,
      init: init,
      auth: getAuthKey()
    };
    return this.http.post(this.appConfig.apiEndpoint + "documents/0/train/", data, AuthOptions()).toPromise()
      .then(response => response)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
