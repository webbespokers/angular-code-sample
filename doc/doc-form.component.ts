import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Doc } from './doc.model';
import { AnalysisType } from '../backend/analysis-type.model';
import { Language } from '../backend/language.model';
import { PdfCleaner } from '../backend/pdf-cleaner.model';
import { TextCleaner } from '../backend/text-cleaner.model';
import { EntityName } from '../entity/entity-name.model';
import { Project } from '../project/project.model';
import { AlertService } from '../alert/alert.service';
import {
  ProjectService, 
  DocService, 
  AnalysisTypeService, 
  LanguageService, 
  PdfCleanerService, 
  TextCleanerService
} from '../backend';
import { EntityService } from '../backend/entity.service';
import { ApiService } from '../utils/api.service';

@Component({
  selector: 'doc-form',
  templateUrl: './doc-form.component.html'
})

export class DocFormComponent implements OnInit {

  @Input() get model() {
    return this._model;
  }
  @Input() buttons: [{nextRoute: string|null, text: string}] = [{nextRoute: null, text: 'Save'}];
  @Input() initProjectId: number;
  @Output() modelChange = new EventEmitter();

  set model(val) {
    if (val.public == null) {
      val.public = true;
    }
    val.project = val.project || null;
    this._model = val;
    const model_project = this.projects && val.project && this.projects.find(p => p.id === val.project.id);
    if (model_project) {
      this.selectProject(model_project);
    }
    this.modelChange.emit(this._model);
  }

  @ViewChild('fileInput') inputEl: ElementRef;

  loading = false;
  nextRoute = null;
  projects: Project[];
  filenames: string;
  analysisTypes: AnalysisType[];
  languages: Language[];
  pdfCleaners: PdfCleaner[];
  textCleaners: TextCleaner[];
  isAdvancedOptVisible: boolean;
  entityNames: EntityName[];
  uploadProgress: number = 0;
  multiple = true;
  model = new Doc({public: true});

  constructor(private router: Router,
              private alertService: AlertService,
              private apiService: ApiService,
              private docService: DocService,
              private projectService: ProjectService,
              private serviceAnalysisType: AnalysisTypeService,
              private serviceLanguage: LanguageService,
              private servicePdfCleaner: PdfCleanerService,
              private serviceTextCleaner: TextCleanerService,
              private serviceEntity: EntityService) {
    this.serviceAnalysisType.getAnalysisTypes().then(analysisTypes => {
      this.analysisTypes = analysisTypes;
    });
    this.serviceLanguage.getLanguages().then(languages => {
      this.languages = languages;
    });
    this.servicePdfCleaner.getPdfCleaners().then(pdfCleaners => {
      this.pdfCleaners = pdfCleaners;
    });
    this.serviceTextCleaner.getTextCleaners().then(textCleaners => {
      this.textCleaners = textCleaners;
    });
    this.serviceEntity.getEntityNames().then(names => this.entityNames = names);
    this.isAdvancedOptVisible = false;
  }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getProjects({docs: false})
      .then(projects => {
        this.projects = projects = projects.filter(p => {
          return p.user_id === this.apiService.me.id
            || this.apiService.me.groups.indexOf('super_user') >= 0
            || this.apiService.me.groups.indexOf('owner') >= 0;
        });
        if (projects && projects.length && this.model && !this.model.project && this.initProjectId != null) {
          const model_project = projects.find(p => p.id === this.initProjectId);
          if (model_project) {
            this.selectProject(model_project);
          }
        }
      })
      .catch(error => this.alertService.error(error));
  }

  updateFile() {
    const inputEl: HTMLInputElement = this.inputEl.nativeElement;
    const files = inputEl.files;
    if (files.length > 0) {
      this.model.file = [];
      const names = [];
      for (let kFile = 0; kFile < inputEl.files.length; ++kFile) {
        const file = inputEl.files.item(kFile);
        this.model.file.push(file);
        names.push(file.name);
      }
      this.filenames = names.join(', ');
    } else {
      this.model.file = null;
      this.filenames = '';
    }
  }

  selectProject(project) {
    this.model.entities = project.entities;
    this.model.language = project.language;
    this.model.analysis_type = project.analysis_type;
    this.model.pdf_cleaner = project.pdf_cleaner;
    this.model.text_cleaner = project.text_cleaner;
    this.model.ml_immediately = project.ml_immediately;
    this.model.convert_immediately = project.convert_immediately;
    this.model.html = project.html;

    this.model.categories = [].concat(project && project.categories || []);
    this.model.project = project;
  }

  onSubmit() {
    this.loading = true;
    const form_data = new FormData(), post_model = this.model.forPost();
    this.model.file.forEach(f => form_data.append('files', f));
    delete post_model.file;
    form_data.append('json_data', JSON.stringify(post_model));
    if (!this.model.id) {
      this.docService.addDoc(this.model, form_data).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const percentDone = Math.round(100 * event.loaded / event.total);
          this.uploadProgress = percentDone;
        } else if (event instanceof HttpResponse) {
          this.uploadProgress = 0;
          this.model = new Doc(post_model);
          this.docService.emitDoc(this.model, 'create');
          this.afterDocSave(post_model);
        }
      });
    } else {
      this.docService.saveDoc(this.model, form_data)
      .then(
        data => {
          this.model = new Doc(post_model);
          this.afterDocSave(post_model);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    }
  }

  afterDocSave(post_model: Doc) {
    this.alertService.success('Document successfully saved.');
    this.loading = false;
    this.inputEl.nativeElement.value = '';
    this.filenames = '';
    if (this.nextRoute) {
      if (this.nextRoute === '$PROJECT') {
        this.nextRoute = ['/home/projects/' + this.model.project.id];
      }
      this.router.navigate(this.nextRoute);
    }
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}


