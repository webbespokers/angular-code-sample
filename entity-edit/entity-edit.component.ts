import { EntityService } from "../backend/entity.service";
import { EntityName } from "../entity/entity-name.model";
import { ApiService } from "../utils/api.service";
import { FormHelperDirective } from "../utils/form-helper.directive";
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-entity-edit',
  templateUrl: './entity-edit.component.html',
  styleUrls: ['./entity-edit.component.scss']
})
export class EntityEditComponent {

  @ViewChild('tmplEditEntity') tmplEditEntity;

  selectedEntity: EntityName;
  entityEditFormErrors = {
    name: {required: 'Name is required.'}
  };

  constructor(
    private entityService: EntityService,
    private modalService: NgbModal,
  ) {}

  selectEntity(orig_entity: EntityName) {
    const entity = (this.selectedEntity = new EntityName(
      orig_entity || {name: '', complete: false}
    ));
    const modal = this.modalService.open(this.tmplEditEntity, {size: 'lg'});
    modal.result
      .then(updated_entity => {
        Object.assign(orig_entity, updated_entity);
      })
      .catch(() => {});
  }

  saveEntity(fh: FormHelperDirective, c) {
    const selectedEntity = this.selectedEntity;
    if (selectedEntity.id) {
      this.entityService
        .updateEntityName(selectedEntity)
        .then((entity: EntityName) => {
          c(entity);
        })
        .catch(error => {
          fh.setResponseErrors(ApiService.parseError(error).errors);
        });
    } else {
      this.entityService
        .createEntityName(selectedEntity)
        .then((entity: EntityName) => {
          c(entity);
        })
        .catch(error => {
          fh.setResponseErrors(ApiService.parseError(error).errors);
        });
    }
  }
}
