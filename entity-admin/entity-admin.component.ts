import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

import {
  ApiService,
  ApiResponseError,
  FormHelperDirective,
  ConfirmService
} from '../utils';
import { EntityName } from './entity-name.model';
import { Project } from '../project/project.model';
import { EntityService, EntityFilter } from '../backend/entity.service';
import { TruncatePipe } from '../pipes/truncate.pipe';
import { ChartComponent } from '../chart/chart.component';
import { TrainComponent } from '../train/train.component';
import { EntityEditComponent } from '../entity-edit/entity-edit.component';

@Component({
  selector: 'app-entity-admin',
  templateUrl: './entity-admin.component.html',
  styleUrls: ['./entity-admin.component.scss']
})

export class EntityAdminComponent implements OnInit {

  @ViewChild(ChartComponent) private chart: ChartComponent;
  @ViewChild(TrainComponent) private train: TrainComponent;
  @ViewChild(EntityEditComponent) private entityEdit: EntityEditComponent;

  entityList: EntityName[];
  entityFilter: EntityFilter;
  entitySubject: any;

  constructor(
    private entityService: EntityService,
    private toastr: ToastrService,
    private confirm: ConfirmService,
  ) {
    this.entityFilter = {search: ''};
    (this.entitySubject = new Subject<string>()).subscribe(
      value => (this.entityFilter.search = value)
    );
    this.entitySubject
      .debounceTime(300)
      .subscribe(value => this.entitySearch());
  }

  ngOnInit() {
    this.entityService
      .getEntityNames(this.entityFilter)
      .then(entities => (this.entityList = entities));
  }

  entitySearch() {
    this.entityService
      .getEntityNames(this.entityFilter)
      .then(entities => (this.entityList = entities));
  }

  deleteEntity(entity: EntityName) {
    this.confirm
      .open({
        title: 'Deleting a entity',
        message:
        `Are you sure you want to delete the entity "${entity.name}"?`,
        okText: 'Delete',
        okType: 'warning'
      })
      .then(() => {
        this.entityService
          .deleteEntityName(entity)
          .then(() => {
            this.entityList = this.entityList.filter(e => e.id !== entity.id);
            this.toastr.success('Entity was successfully deleted.');
          })
          .catch(e => {
            const error = ApiService.parseError(e);
            const msg =
              (error.message && error.message.detail) ||
              ApiService.fieldError('', error) ||
              'Unknown error';
            this.toastr.warning(msg, error.message && error.message.title);
          });
      })
      .catch(() => {});
  }

  showTrain(entity) {
    this.train.trainInitialOpen(entity);
  }

  showEntityEdit(entity) {
    this.entityEdit.selectEntity(entity);
  }

  showChart(entity) {
    this.chart.showChart(entity);
  }
}
