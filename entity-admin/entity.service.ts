import { Injectable, Inject } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import { APP_CONFIG } from '../app.config';
import { IAppConfig } from '../iappconfig.interface';
import { AuthOptions } from '../auth/auth.service';
import { Doc } from '../doc/doc.model';
import { Entity } from '../entity/entity.model';
import { EntityName } from '../entity/entity-name.model';
import { EntityNameChart } from '../entity/entity-name-chart.model';

export interface EntityFilter {
  search?: string;
}

interface EntitySearch {
  project_id?: number;
  doc_id?: number;
  search: string;
  names?: number[];
  limit?: number;
  offset?: number;
}

export interface EntityNameChartSearch {
  entity_name_id: number;
}

@Injectable()
export class EntityService {

  constructor(@Inject(APP_CONFIG) private appConfig: IAppConfig,
              private http: Http) {
  }

  getEntityNames(filter?: EntityFilter): Promise<EntityName[]> {
    const options = AuthOptions();
    if (filter) {
      options.params = options.params || new URLSearchParams();
      if (filter.search) {
        options.params.set('search', filter.search);
      }
    }
    return this.http.get(this.appConfig.apiEndpoint + 'entity-names/', options).toPromise()
      .then(response => response.json().map(obj => new EntityName(obj)))
      .catch(this.handleError);
  }

  searchEntities(filter: EntitySearch): Promise<Entity[]> {
    const options = AuthOptions();
    const params = options.params = new URLSearchParams();
    if (filter.project_id) {
      params.set('project_id', filter.project_id.toString());
    }
    if (filter.search) {
      params.set('search', filter.search);
    }
    if (filter.names && filter.names.length) {
      params.set('names', filter.names.join(','));
    }
    if (filter.limit != null) {
      params.set('limit', filter.limit.toString());
      params.set('offset', (filter.offset || 0).toString());
    }
    return this.http.get(this.appConfig.apiEndpoint + 'documents/' + (filter.doc_id || 0) + '/entities/search/', options).toPromise()
      .then(response => {
        const data = response.json();
        const paged = filter && filter.limit != null;
        const res = (paged ? data.results : data).map(obj => new Entity(obj));
        if (paged) {
          res.push(data.next);
        }
        return res;
      })
      .catch(this.handleError);
  }

  createEntityName(entity_name: EntityName): Promise<EntityName> {
    return this.http.post(this.appConfig.apiEndpoint + 'entity-names/', entity_name, AuthOptions()).toPromise()
      .then(response => new EntityName(response.json()))
      .catch(this.handleError);
  }

  updateEntityName(entity_name: EntityName): Promise<EntityName> {
    return this.http.patch(this.appConfig.apiEndpoint + 'entity-names/' + entity_name.id + '/', entity_name, AuthOptions()).toPromise()
      .then(response => new EntityName(response.json()))
      .catch(this.handleError);
  }

  deleteEntityName(entity_name: EntityName): Promise<EntityName> {
    return this.http.delete(this.appConfig.apiEndpoint + 'entity-names/' + entity_name.id + '/',
      AuthOptions())
      .toPromise()
      .then(() => entity_name)
      .catch(err => this.handleError(err));
  }

  getEntitiesForDoc(doc: Doc): Promise<Entity[]> {
     return this.http.get(this.appConfig.apiEndpoint + 'documents/' + doc.id + '/entities/', AuthOptions()).toPromise()
      .then(response => response.json().map(obj => new Entity(obj)))
      .catch(this.handleError);
  }

  createDocEntity(doc: Doc, entity: Entity): Promise<Entity> {
    return this.http.post(this.appConfig.apiEndpoint + 'documents/' + doc.id + '/entities/', entity, AuthOptions()).toPromise()
      .then(response => new Entity(response.json()))
      .catch(this.handleError);
  }

  saveNames(doc: Doc, entity: Entity, names: EntityName[]): Promise<Entity> {
    const data = {
      id: entity.id,
      names: names
    };
    return this.http.patch(this.appConfig.apiEndpoint + 'documents/' + doc.id + '/entities/' + entity.id + '/',
      data, AuthOptions()).toPromise()
      .then(response => {
        entity.names = names;
        return new Entity(response.json());
      })
      .catch(err => {
        this.handleError(err);
      });
  }


  patchEntity(doc: Doc, entity: Entity): Promise<Entity> {
    let data = {};
    data = entity;

    return this.http.patch(this.appConfig.apiEndpoint + 'documents/' + doc.id + '/entities/' + entity.id + '/', data, AuthOptions())
      .toPromise()
      .then(response => new Entity(response.json()))
      .catch(this.handleError);
  }


  removeEntity(doc: Doc, entity: Entity): Promise<Entity> {
    return this.http.delete(this.appConfig.apiEndpoint + 'documents/' + doc.id + '/entities/' + entity.id + '/',
      AuthOptions())
      .toPromise()
      .then(() => entity)
      .catch(err => this.handleError(err));
  }

  getEntityNameChart(filter?: EntityNameChartSearch): Promise<EntityNameChart[]> {
    const options = AuthOptions();
    const params = options.params = new URLSearchParams();

    if (filter.entity_name_id) {
      params.set('entity_name_id', filter.entity_name_id.toString());
    }
    return this.http.get(this.appConfig.apiEndpoint + 'entity-name-chart/', options).toPromise()
      .then(response => response.json().map(obj => new EntityNameChart(obj)))
      .catch(this.handleError);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
